<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Anastasia Melikhova 27/2 </title>
  </head>
  <body>
      <header>
    <h1>Форма</h1>
  </header>
  <div class="my-blocks">
    <div id="form">
    <form action="" method="POST">
      <label>
        Введите ваше имя:<br />
        <input name="name"
        placeholder="Анастасия" />
      </label><br />

      <label>
        Введите ваш email:<br />
        <input name="mail"
        placeholder="test@example.com"
          type="email" />
      </label><br />
    
      <label>
          <p>Ваш год рождения:</p>
          <select name="year">
              <option value="1990">1990</option>
              <option value="1991">1991</option>
              <option value="1992">1992</option>
              <option value="1993">1993</option>
              <option value="1994">1994</option>
              <option value="1995">1995</option>
              <option value="1996">1996</option>
              <option value="1997">1997</option>
              <option value="1998">1998</option>
              <option value="1999">1999</option>
              <option value="2000">2000</option>
              <option value="2001" selected="selected">2001</option>
              <option value="2002">2002</option>
              <option value="2003">2003</option>
              <option value="2004">2004</option>
              <option value="2005">2005</option>
              <option value="2006">2006</option>
              <option value="2007">2007</option>
              <option value="2008">2008</option>
              <option value="2009">2009</option>
              <option value="2010">2010</option>
            </select>
      </label><br />
    
      <label>      
        Ваш пол:<br />
        <input type="radio" checked="checked"
          name="sex" value="M" />
          Мужской
      </label>
      <label>
        <input type="radio"
          name="sex" value="F" />
          Женский
      </label><br />

      <label>    
        Кол-во конечностей:<br />
        <input type="radio" checked="checked"
          name="countlimbs" value="4" />
          4
      </label>
      <label><input type="radio" 
          name="countlimbs" value="5" />
          5
      </label>
      <label><input type="radio" 
          name="countlimbs" value="2" />
          2
      </label>
      <label><input type="radio"
          name="countlimbs" value="16" />
          16
      </label><br />
    
      <label>
          Какие у вас есть сверхспособности:
          <br />
          <select name="super[]"
          multiple="multiple">
              <option value="Reading your mind" selected="selected">Чтение мыслей</option>
              <option value="Predict weather" selected="selected">Предсказывание погоды</option>
              <option value="Passing through the wall" selected="selected">Прохождение сквозь стену</option>
              <option value="Immortalitty" selected="selected">Бессмертие</option>
              <option value="Levitation" selected="selected">Левитация</option>
          </select>
      </label><br />
    
      <label>
          Ваша биография:<br />
          <textarea name="biography"></textarea>
      </label><br />
    
        Соглашение:<br />
      <label><input type="checkbox" checked="checked"
      name="check1" />
      С контрактом ознакомлен
      </label><br />
    
      <input type="submit" value="Отправить" />
    </form>
  </div>
</div>
<footer>
</footer>
</body>
</html>
